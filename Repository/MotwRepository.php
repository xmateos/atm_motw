<?php

namespace ATM\MotwBundle\Repository;

use Doctrine\ORM\EntityRepository;
use ATM\MotwBundle\Entity\Motw;
use \DateTime;
use \DateInterval;
use \DatePeriod;

class MotwRepository extends EntityRepository{

    public function getForbiddenDates($start_date, $end_date)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $items = $qb
            ->select('partial m.{id, init_date, end_date}')
            ->from(Motw::class,'m')
            ->where($qb->expr()->andX(
                $qb->expr()->lte('DATE(m.init_date)', $qb->expr()->literal($end_date->format('Y-m-d'))),
                $qb->expr()->gte('DATE(m.end_date)', $qb->expr()->literal($start_date->format('Y-m-d')))
            ))
            ->orderBy('m.init_date','ASC')
            ->getQuery()
            ->getArrayResult()
        ;

        $forbidden_dates = [];
        foreach($items as $motw)
        {
            $start = $motw['init_date'];
            $end = $motw['end_date'];
            $end = $end->modify('+1 day'); // Because the end date is exclusive in DatePeriod
            $interval = new DateInterval('P1D'); // 1 day interval
            $dateRange = new DatePeriod($start, $interval, $end);

            foreach ($dateRange as $date) {
                $forbidden_dates[] = $date->format('d-m-Y');
            }
        }
        return array_unique($forbidden_dates);
    }
}