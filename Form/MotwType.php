<?php

namespace ATM\MotwBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
//use MM\CoreBundle\Form\CustomFormFields\AutocompleteCollectionType;
use ATM\MotwBundle\Entity\Motw;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\CallbackTransformer;
use XLabs\TrumboWYGBundle\Form\TrumboWYGType;

class MotwType extends AbstractType
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $qb = $this->em->createQueryBuilder();
        $entities = $qb
            ->select('partial u.{id, username, title}')
            ->from($options['user'], 'u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->like('u.roles', $qb->expr()->literal('%MODEL%')),
                    $qb->expr()->eq('u.enabled',1),
                    $qb->expr()->eq('u.approved',1),
                    $qb->expr()->eq('u.locked',0)
                )
            )
            ->orderBy('u.username', 'ASC')
            ->getQuery()
            ->getArrayResult()
        ;

        $choices = [];
        foreach($entities as $entity)
        {
            $username = $entity['username'];
            $display_name = $entity['title'] ? $entity['title'] : false;
            $choices[($display_name ? $display_name : $username).($display_name ? (' ['.$username.']') : '')] = $entity['id'];
        }

        $builder
            /*->add('model',AutocompleteCollectionType::class, array(
                'required' => false,
                'attr' => array(
                    'data-autocomplete' => array(
                        'class' => $options['user'],
                        'repository_function' => 'getFormCollection',
                        'repository_function_params' => array(
                            'roles' => array(
                                'ROLE_AFFILIATE',
                                'ROLE_MODEL',
                                'ROLE_STUDIO'
                            )
                        )
                    )
                )
            ))*/
            ->add('model',ChoiceType::class,array(
                //'label' => 'Sorting',
                //'label_attr' => ['class' => 'form-label'],
                'choices' => $choices,
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'Choose a model',
                //'attr' => ['class' => 'form-select form-select-sm']
            ))
            /*->add('model',EntityType::class, array(
                'class' => $options['user'],
                'query_builder' => function($er) {
                    $qb = $er->createQueryBuilder('u');
                    return $qb
                        //->select('partial u.{id, username}')
                        ->where(
                            $qb->expr()->andX(
                                $qb->expr()->like('u.roles', $qb->expr()->literal('%MODEL%')),
                                $qb->expr()->eq('u.enabled',1),
                                $qb->expr()->eq('u.approved',1),
                                $qb->expr()->eq('u.locked',0)
                            )
                        )
                        ->orderBy('u.username', 'ASC');
                },
                'choice_label' => 'username',
                'placeholder' => 'Choose a model',
                'empty_data'  => null,
                'required' => true
            ))*/
            ->add('description', TrumboWYGType::class,array(
                'required' => true,
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Write something'
                ),
                'plugin_options' => [
                    'btns' => [['bold', 'italic', 'underline'], ['link'], ['viewHTML']],
                    'autogrow' => true,
                    'tagsToRemove' => ['script', 'link', 'iframe']
                ]
            ))
            /*->add('description',TextareaType::class,array(
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Description',
                    'autocomplete' => 'off'
                ),
            ))*/
            ->add('init_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Init Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('end_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'End Date',
                    'autocomplete' => 'off'
                ),
            ))
        ;

        $builder->get('model')
            ->addModelTransformer(new CallbackTransformer(
                function($val) {
                    return is_null($val) ? '' : $val->getId();
                },
                function($val) use ($options) {
                    return is_null($val) ? '' : $this->em->getRepository($options['user'])->find($val);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user' => null,
            'data' => Motw::class
        ));
    }
}