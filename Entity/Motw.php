<?php

namespace ATM\MotwBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ATM\MotwBundle\Repository\MotwRepository")
 * @ORM\Table(name="atm_motw")
 */
class Motw{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    protected $model;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="init_date", type="datetime")
     */
    private $init_date;

    /**
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $end_date;

    /**
     * @ORM\Column(name="thumbnail_members", type="text", nullable=false)
     */
    private $thumbnail_members;

    /**
     * @ORM\Column(name="thumbnail_members_mobile", type="text", nullable=false)
     */
    private $thumbnail_members_mobile;

    /**
     * @ORM\Column(name="thumbnail_tour", type="text", nullable=false)
     */
    private $thumbnail_tour;

    /**
     * @ORM\Column(name="thumbnail_tour_mobile", type="text", nullable=false)
     */
    private $thumbnail_tour_mobile;

    /**
     * @ORM\Column(name="folderName", type="string", length=255, nullable=false)
     */
    private $folderName;

    public function __construct()
    {
        $this->creation_date = new DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getInitDate()
    {
        return $this->init_date;
    }

    public function setInitDate($init_date)
    {
        $this->init_date = $init_date;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    public function getThumbnailMembers()
    {
        return $this->thumbnail_members;
    }

    public function setThumbnailMembers($thumbnail_members)
    {
        $this->thumbnail_members = $thumbnail_members;
    }

    public function getThumbnailTour()
    {
        return $this->thumbnail_tour;
    }

    public function setThumbnailTour($thumbnail_tour)
    {
        $this->thumbnail_tour = $thumbnail_tour;
    }

    public function getFolderName()
    {
        return $this->folderName;
    }

    public function setFolderName($folderName)
    {
        $this->folderName = $folderName;
    }

    public function getThumbnailMembersMobile()
    {
        return $this->thumbnail_members_mobile;
    }

    public function setThumbnailMembersMobile($thumbnail_members_mobile)
    {
        $this->thumbnail_members_mobile = $thumbnail_members_mobile;
    }

    public function getThumbnailTourMobile()
    {
        return $this->thumbnail_tour_mobile;
    }

    public function setThumbnailTourMobile($thumbnail_tour_mobile)
    {
        $this->thumbnail_tour_mobile = $thumbnail_tour_mobile;
    }
}