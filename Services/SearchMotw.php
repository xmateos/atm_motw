<?php

namespace ATM\MotwBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class SearchMotw{

    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function search($options)
    {
        $defaultOptions = array(
            'date' => null,
            'ids' => null,
            'models_id' => null,
            'sorting' => 'init_date',
            'sorting_direction' => 'DESC',
            'pagination' => null,
            'max_results' => null,
            'page' => 1
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('m.id')
            ->from('ATMMotwBundle:Motw','m');

        if (!is_null($options['models_id'])) {
            $qbIds->join('m.model','mo','WITH',$qbIds->expr()->in('mo.id',$options['models_id']));
        }else{
            $qbIds->join('m.model','mo');
        }


        if (!is_null($options['ids'])) {
            $qbIds
                ->andWhere($qbIds->expr()->notIn('m.id', $options['ids']));
        }

        if (!is_null($options['date'])) {
            $qbIds->andWhere($qbIds->expr()->lte('DATE(m.init_date)', $qbIds->expr()->literal($options['date'])));
        }

        $qbIds->andWhere($qbIds->expr()->eq('mo.enabled',1));
        $qbIds->orderBy('m.'.$options['sorting'],$options['sorting_direction']);

        $pagination = null;
        if (!is_null($options['pagination'])) {
            $arrIds = array_map(function ($p) {
                return $p['id'];
            }, $qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        } else {
            $queryIds = $qbIds->getQuery();
            if (!is_null($options['max_results'])) {
                $queryIds->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $queryIds->getArrayResult());
        }

        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('m')
                ->addSelect('mo')
                ->from('ATMMotwBundle:Motw','m')
                ->join('m.model','mo')
                ->where($qb->expr()->in('m.id',$ids))
                ->orderBy('m.'.$options['sorting'],$options['sorting_direction']);
            $motws = $qb->getQuery()->getArrayResult();
        }else{
            $motws = array();
        }


        return array(
            'results' => $motws,
            'pagination' => $pagination
        );


    }
}