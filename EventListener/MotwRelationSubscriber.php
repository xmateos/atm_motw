<?php

namespace ATM\MotwBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;


class MotwRelationSubscriber implements EventSubscriber{

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if ($metadata->getName() != 'ATM\MotwBundle\Entity\Motw') {
            return;
        }

        $metadata->mapManyToOne(array(
            'targetEntity' => $this->config['user'],
            'fieldName' => 'model',
            'joinColumns' => array(
                array(
                    'name' => 'model_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));
    }

}