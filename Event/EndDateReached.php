<?php

namespace ATM\MotwBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class EndDateReached extends Event
{
    const NAME = 'atm.motw.end_date.event';

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}