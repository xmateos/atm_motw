<?php

namespace ATM\MotwBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class InitDateReached extends Event
{
    const NAME = 'atm.motw.init_date.event';

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}