<?php

namespace ATM\MotwBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ATM\MotwBundle\Entity\Motw;
use ATM\MotwBundle\Event\InitDateReached;
use ATM\MotwBundle\Event\EndDateReached;
use \DateTime;

class DatesReachedCommand extends ContainerAwareCommand
{
    private $container;
    private $em;
    private $event_dispatcher;

    /**
     * Search for MOTW that begins/finished on the current date, to fire an event
     */
    protected function configure()
    {
        $this
            ->setName('atm:motw:events')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $kernel = $this->getApplication()->getKernel();
        $this->container = $kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->event_dispatcher = $this->container->get('event_dispatcher');

        $today = new DateTime();
        // First day model of the week
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('motw')
            ->from(Motw::class, 'motw')
            ->where(
                $qb->expr()->eq('DATE(motw.init_date)', $qb->expr()->literal($today->format('Y-m-d')))
            )
        ;
        $begins_today = $qb->getQuery()->getResult();
        foreach($begins_today as $motw) // should be only one
        {
            $event = new InitDateReached(array(
                'motw' => $motw,
                'timestamp' => time()
            ));
            $this->event_dispatcher->dispatch($event::NAME, $event);
        }

        // Last day model of the week
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('motw')
            ->from(Motw::class, 'motw')
            ->where(
                $qb->expr()->eq('DATE(motw.end_date)', $qb->expr()->literal($today->format('Y-m-d')))
            )
        ;
        $ends_today = $qb->getQuery()->getResult();
        foreach($ends_today as $motw) // should be only one
        {
            $event = new EndDateReached(array(
                'motw' => $motw,
                'timestamp' => time()
            ));
            $this->event_dispatcher->dispatch($event::NAME, $event);
        }
    }
}